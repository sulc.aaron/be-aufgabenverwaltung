<?php

namespace Database\Seeders;

use App\Models\Aufgabe;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AufgabenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Aufgabe::factory()->create([
            'titel' => 'Räume überprüfen',
            'beschreibung' => 'Alle Räume sollten nach dem Ende der Geschäftszeit abgeschlossen sein. Überprüfe, ob alle Räume ordnungsgemäß abgeschlossen sind.',
        ]);
        Aufgabe::factory()->create([
            'titel' => 'Paket versenden',
            'beschreibung' => 'Versende das Paket mit der AuftragsID 123abcd.',
        ]);
        Aufgabe::factory()->create([
            'titel' => 'Materialien nachbestellen',
            'beschreibung' => 'Bestelle alle in der Liste festgehaltenen Materialien nach.',
        ]);
        Aufgabe::factory(4)->create();
    }
}
