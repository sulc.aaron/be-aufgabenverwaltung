<?php

namespace App\Http\Controllers;

use App\Models\Aufgabe;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class AufgabenController extends BaseController
{
    function getAll(Request $request)
    {
        $aufgaben = Aufgabe::all('id', 'titel', 'beschreibung');

        return response()->json($aufgaben);
    }

    function edit(Request $request)
    {
        $data = request()->only('id', 'titel', 'beschreibung');
        $valid = validator($data, [
            'id' => 'required|int',
            'titel' => 'required|string|max:50',
            'beschreibung' => 'required|string|max:255',
        ]);

        if ($valid->fails()) {
            return response()->json($valid->errors()->all(), 400);
        }

        $aufgabe = Aufgabe::query()->findOrFail($data['id']);

        $aufgabe->titel = $data['titel'];
        $aufgabe->beschreibung = $data['beschreibung'];
        $aufgabe->save();

        return response()->json();
    }

    function create(Request $request)
    {
        $data = request()->only('titel', 'beschreibung');
        $valid = validator($data, [
            'titel' => 'required|string|max:50',
            'beschreibung' => 'required|string|max:255',
        ]);

        if ($valid->fails()) {
            return response()->json($valid->errors()->all(), 400);
        }

        $aufgabe = Aufgabe::create([
            'titel' => $data['titel'],
            'beschreibung' => $data['beschreibung'],
        ]);

        return response()->json([], 201);
    }

    function delete(Request $request)
    {
        $data = request()->only('id');
        $valid = validator($data, [
            'id' => 'required|int',
        ]);

        if ($valid->fails()) {
            return response()->json($valid->errors()->all(), 400);
        }

        $aufgabe = Aufgabe::query()->findOrFail($data['id']);
        $aufgabe->delete();

        return response()->json();
    }
}
