<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group([], function ($router) {
    Route::get('/aufgaben', 'App\Http\Controllers\AufgabenController@getAll');
    Route::post('/aufgabe/anpassen', 'App\Http\Controllers\AufgabenController@edit');
    Route::post('/aufgabe/erstellen', 'App\Http\Controllers\AufgabenController@create');
    Route::post('/aufgabe/entfernen', 'App\Http\Controllers\AufgabenController@delete');
});
